// index.js

/*
	- To create a class in JS, we use the class keyword and {}
	- Naming convention for classes: Begins with a capital letter

	- We add a constructor method to a class to be able to initialize values upon instantiation of an object from a class

	- Instantiation > erm when creating an object from a class
	- Instant > an object created from a class
	- "new" keyword is used to instantiate an object from a class

	- Chaining Methods > we need to return "this" 
*/

class Dog {

	constructor(name,breed,age){

		this.name = name;
		this.breed = breed;
		this.age = age * 7;
	}
};

let dog1 = new Dog("Bantay","chihuahua",3);

console.log(dog1);


class Person {

	constructor(name,age,nationality,address){

		this.name = name;
		this.age = age;
		this.nationality = nationality;
		this.address = address;

		/* - moved these below to add separately -
		this.greet =  function(){
			console.log(`Hello! Good Morning!`);
		};
		this.introduce = function(){
			console.log(`Hi! My name is ${this.name}`)
		};
		*/

		/* - added validation - */
		if(typeof age !== 'number'){
			this.age = undefined;
		} else {
			this.age = age;
		}
	};

	/* - added methods that would show under prototype - */
	greet(){
		console.log(`Hello! Good Morning!`);
		return this;
	};

	introduce(){
		console.log(`Hi! My name is ${this.name}.`)
		return this;
	};

	changeAddress(newAddress){
		this.address = newAddress;
		return this;
	}
	
}
	

let person1 = new Person("John",20,"Dutch","Amsterdam");
let person2 = new Person("Jack",21,"Irish","Galway");

console.log(person1);
console.log(person2);


/*======== ACTIVITY #1 ========*/

class Student {

	constructor(name,email,grades){

		this.name = name;
		this.email = email;
		this.average = undefined;
		this.isPassed = undefined;
		this.isPassedWithHonors = undefined;

	   if(grades.every(number => { return typeof number === 'number'})){
			let i = 0;
			let gradearray = [];
			while(i < grades.length){
				gradearray.push(grades[i])
				i++
			}
	   	this.grades = gradearray
	   } else {
	   	this.grades = undefined;	
	   };
	}

   login(){
   	console.log(`${this.email} has logged in.`);
   	return this;
   };
   logout(){
   	console.log(`${this.email} has logged out.`)
   	return this;
   };
   listGrades(){
   	this.grades.forEach(grade => {
   		console.log(grade);
   	});
   	return this;	
   };
   computeAve(){
   	let sum = 0;
   	this.grades.forEach(grade => {
   		sum += grade;
   	})
   	this.average = sum/this.grades.length
   	return this;
   };
   willPass(){
   	this.isPassed = Math.round(this.average) >= 85 ? true : false;
   	return this;
   };
   willPassWithHonors(){
   	this.isPassedWithHonors = (this.willPass() && Math.round(this.average) >= 90) ? true : this.willPass() ? false : undefined;
   	return this;
   }
};

let student1 = new Student('John','john@mail.com',[89, 84, 78, 88])
let student2 = new Student('Joe','joe@mail.com',[78, 82, 79, 85])
let student3 = new Student('Jane','jane@mail.com',[87, 89, 91, 93])
let student4 = new Student('Jessie','jessie@mail.com',[91, 89, 92, 93])

console.log(student1);
console.log(student2);
console.log(student3);
console.log(student4);